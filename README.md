This is a drop in replacement for the auth0 java-jwt library (see https://github.com/auth0/java-jwt). This jar makes sure there are no external dependencies (e.g. fasterXml, Apacha Commons) needed. This is useful when deploying to an application server (e.g. tomcat with Alfreso or Pega).

To use this jar, replace the current dependency
```xml
<dependency>
  <groupId>com.auth0</groupId>
  <artifactId>java-jwt</artifactId>
  <version>3.2.0</version>
</dependency>
```
with:
```xml
<dependency>
  <groupId>nl.open</groupId>
  <artifactId>java-jwt-nodependencies</artifactId>
  <version>3.2.0</version>
</dependency>
```
